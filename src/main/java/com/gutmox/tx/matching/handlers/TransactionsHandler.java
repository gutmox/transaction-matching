package com.gutmox.tx.matching.handlers;

import com.google.inject.Inject;
import com.gutmox.tx.matching.transactions.api.TransactionsApi;
import com.gutmox.tx.matching.transactions.domain.MatchTransactions;
import com.gutmox.tx.matching.transactions.domain.Matched;
import com.gutmox.tx.matching.transactions.domain.Transaction;
import com.gutmox.tx.matching.transactions.impl.TransactionsImpl;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.reactivex.ext.web.RoutingContext;

import java.util.stream.Collectors;

public class TransactionsHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(TransactionsHandler.class);

    private TransactionsApi transactionsApi;

    @Inject
    public TransactionsHandler(TransactionsImpl transactionsApi) {
        this.transactionsApi = transactionsApi;
    }

    public void getAll(RoutingContext context) {

        String state = context.request().getParam("state");

        LOGGER.info("transactions by state " + state);

        transactionsApi.getAll(state)
                .doOnError(err -> {
                    returnError(context, err);
                })
                .subscribe(transactions -> {

                    JsonArray jsonArray = new JsonArray();
                    transactions.stream().map(Transaction::toJson).collect(Collectors.toList()).forEach(jsonArray::add);
                    context.response().putHeader("content-type", "application/json")
                            .end(jsonArray.encode());
                });
    }

    public void getOne(RoutingContext context) {

        String transactionIdentifier = context.request().getParam("transactionIdentifier");

        transactionsApi.getOne(transactionIdentifier)
                .doOnError(err -> {
                    returnError(context, err);
                })
                .subscribe(transaction -> {

                    LOGGER.info("Result : " + transaction);

                    context.response().putHeader("content-type", "application/json")
                            .end(transaction.toJson().encode());
                });
    }

    private void returnError(RoutingContext context, Throwable err) {
        context.response().putHeader("content-type", "application/json")
                .end(new JsonObject().put("error", err.getMessage()).encode());
    }

    public void getStats(RoutingContext context) {

        transactionsApi.getStats()
                .doOnError(err -> {
                    returnError(context, err);
                })
                .subscribe(stats -> {

                    LOGGER.info("Stats : " + stats);

                    context.response().putHeader("content-type", "application/json")
                            .end(stats.toJson().encode());
                });

    }

    public void match(RoutingContext context) {

        LOGGER.info("Match : " + context.getBodyAsString());

        transactionsApi.match(MatchTransactions.fromJson(context.getBodyAsJson()))
                .subscribe(() -> {
                    // Transaction succeeded
                    context.response().putHeader("content-type", "application/json")
                            .end(Matched.builder().withMatched(true).build().toJson().encode());
                }, err -> {
                    // Transaction failed
                    context.response().putHeader("content-type", "application/json")
                            .end(Matched.builder().withMatched(false).build().toJson().encode());
                });
    }

    public void unmatch(RoutingContext context) {

        LOGGER.info("Unmatch : " + context.getBodyAsString());

        transactionsApi.unmatch(MatchTransactions.fromJson(context.getBodyAsJson()))
                .subscribe(() -> {
                    // Transaction succeeded
                    context.response().putHeader("content-type", "application/json")
                            .end(Matched.builder().withMatched(false).build().toJson().encode());
                }, err -> {
                    // Transaction failed
                    context.response().putHeader("content-type", "application/json")
                            .end(Matched.builder().withMatched(true).build().toJson().encode());
                });
    }
}
