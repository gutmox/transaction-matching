package com.gutmox.tx.matching.handlers;

import com.google.inject.Inject;
import com.gutmox.tx.matching.merchants.api.MerchantsApi;
import com.gutmox.tx.matching.merchants.domain.Merchant;
import com.gutmox.tx.matching.merchants.impl.MerchantsApiImpl;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.reactivex.ext.web.RoutingContext;

import java.util.stream.Collectors;

public class MerchantsHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(MerchantsHandler.class);

    private MerchantsApi merchantsApi;

    @Inject
    public MerchantsHandler(MerchantsApiImpl merchantsApi) {
        this.merchantsApi = merchantsApi;
    }

    public void getAll(RoutingContext context) {

        String name = context.request().getParam("name");

        LOGGER.info("merchant name " + name);

        merchantsApi.getAll(name)
                .doOnError(err -> {
                    context.response().putHeader("content-type", "application/json")
                            .end(new JsonObject().put("error", err.getMessage()).encode());
                })
                .subscribe(merchants -> {

                    LOGGER.info("Result : " + merchants);
                    JsonArray jsonArray = new JsonArray();
                    merchants.stream().map(Merchant::toJson).collect(Collectors.toList()).forEach(jsonArray::add);
                    context.response().putHeader("content-type", "application/json")
                            .end(jsonArray.encode());
                });
    }


    public void getOne(RoutingContext context) {

        String merchantIdentifier = context.request().getParam("merchantIdentifier");

        merchantsApi.getOne(merchantIdentifier)
                .doOnError(err -> {
                    context.response().putHeader("content-type", "application/json")
                            .end(new JsonObject().put("error", err.getMessage()).encode());
                })
                .subscribe(merchant -> {

                    LOGGER.info("Result : " + merchant);

                    context.response().putHeader("content-type", "application/json")
                            .end(merchant.toJson().encode());
                });
    }

}
