package com.gutmox.tx.matching.transactions.api.exceptions;

public class InvalidStateException extends RuntimeException{

    public InvalidStateException(String message) {
        super(message);
    }
}
