package com.gutmox.tx.matching.transactions.impl;

import com.google.inject.Inject;
import com.gutmox.tx.matching.transactions.api.TransactionsApi;
import com.gutmox.tx.matching.transactions.api.exceptions.InvalidStateException;
import com.gutmox.tx.matching.transactions.domain.*;
import com.gutmox.tx.matching.transactions.repositories.TransactionsRepository;
import io.reactivex.Completable;
import io.reactivex.Single;

import java.util.List;
import java.util.Optional;

public class TransactionsImpl implements TransactionsApi {

    private TransactionsRepository transactionsRepository;

    @Inject
    public TransactionsImpl(TransactionsRepository transactionsRepository) {
        this.transactionsRepository = transactionsRepository;
    }

    @Override
    public Single<List<Transaction>> getAll(String state) {
        if (!Optional.ofNullable(state).isPresent()) {
            return transactionsRepository.getAll();
        } else {
            try {
                TransactionState transactionState = TransactionState.valueOf(state);
                return transactionsRepository.getByState(transactionState);
            } catch(IllegalArgumentException ex){
                throw new InvalidStateException(ex.getMessage());
            }
        }
    }

    @Override
    public Single<Transaction> getOne(String transactionIdentifier) {
        return transactionsRepository.getOne(transactionIdentifier);
    }

    @Override
    public Single<TransactionStats> getStats() {
        return transactionsRepository.getStats();
    }

    @Override
    public Completable match(MatchTransactions matchTransactions) {
        return transactionsRepository.match(matchTransactions.getTransactionOne(),
                matchTransactions.getTransactionTwo());
    }

    @Override
    public Completable unmatch(MatchTransactions matchTransactions) {
        return transactionsRepository.unmatch(matchTransactions.getTransactionOne(),
                matchTransactions.getTransactionTwo());
    }
}
