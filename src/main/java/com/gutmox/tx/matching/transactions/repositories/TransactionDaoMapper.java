package com.gutmox.tx.matching.transactions.repositories;

import com.gutmox.tx.matching.pgclient.DaoMapper;
import com.gutmox.tx.matching.transactions.domain.Transaction;
import com.gutmox.tx.matching.transactions.domain.TransactionState;
import io.reactiverse.reactivex.pgclient.Row;

import java.util.concurrent.atomic.AtomicInteger;

public class TransactionDaoMapper implements DaoMapper<Transaction> {

    @Override
    public Transaction map(Row row) {
        return Transaction.builder()
                .withId(row.getUUID("id").toString())
                .withMerchantId((row.getUUID("merchant_id") == null) ? "" : row.getUUID("merchant_id").toString())
                .withLocationId((row.getUUID("location_id") == null) ? "" : row.getUUID("location_id").toString())
                .withCardScheme(row.getString("card_scheme"))
                .withBin(row.getString("bin"))
                .withLastFour(row.getString("last_four"))
                .withProvider(row.getString("provider"))
                .withSource(row.getString("source"))
                .withAmount(row.getDouble("amount"))
                .withState(TransactionState.valueOf(row.getString("state")))
                .withTransactionDate(row.getLocalDateTime("transaction_date"))
                .withCreatedDate(row.getLocalDateTime("created_date"))
                .withUpdatedDate(row.getLocalDateTime("updated_date"))
                .withMerchantName(row.getString("merchant_name"))
                .withMatchedWith((row.getUUID("matched_with") == null) ? "" : row.getUUID("matched_with").toString())
                .build();
    }

    public void stats(AtomicInteger matched, AtomicInteger unmatched, Row row) {

        String state = row.getString("state");
        if (state.equals(TransactionState.MATCHED.name())) {
            matched.addAndGet(row.getInteger("count"));
        } else if (state.equals(TransactionState.UNMATCHED.name())) {
            unmatched.addAndGet(row.getInteger("count"));
        }
    }

}
