package com.gutmox.tx.matching.transactions.domain;

import io.vertx.core.json.JsonObject;

import java.time.LocalDateTime;

public class Transaction {

    private JsonObject jsonObject;

    private Transaction() {
        this.jsonObject = jsonObject;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private String id;
        private String merchantId;
        private String locationId;
        private String cardScheme;
        private String bin;
        private String lastFour;
        private String provider;
        private String source;
        private Double amount;
        private LocalDateTime transactionDate;
        private TransactionState state;
        private LocalDateTime createdDate;
        private LocalDateTime updatedDate;
        private String merchantName;
        private String matchedWith;

        private Builder() {
        }

        public Builder withId(String id) {
            this.id = id;
            return this;
        }

        public Builder withMerchantId(String merchantId) {
            this.merchantId = merchantId;
            return this;
        }

        public Builder withLocationId(String locationId) {
            this.locationId = locationId;
            return this;
        }

        public Builder withCardScheme(String cardScheme) {
            this.cardScheme = cardScheme;
            return this;
        }

        public Builder withBin(String bin) {
            this.bin = bin;
            return this;
        }

        public Builder withLastFour(String lastFour) {
            this.lastFour = lastFour;
            return this;
        }

        public Builder withProvider(String provider) {
            this.provider = provider;
            return this;
        }

        public Builder withSource(String source) {
            this.source = source;
            return this;
        }

        public Builder withAmount(Double amount) {
            this.amount = amount;
            return this;
        }

        public Builder withTransactionDate(LocalDateTime transactionDate) {
            this.transactionDate = transactionDate;
            return this;
        }

        public Builder withState(TransactionState state) {
            this.state = state;
            return this;
        }

        public Builder withCreatedDate(LocalDateTime createdDate) {
            this.createdDate = createdDate;
            return this;
        }

        public Builder withUpdatedDate(LocalDateTime updatedDate) {
            this.updatedDate = updatedDate;
            return this;
        }

        public Builder withMerchantName(String merchantName) {
            this.merchantName = merchantName;
            return this;
        }

        public Builder withMatchedWith(String matchedWith) {
            this.matchedWith = matchedWith;
            return this;
        }

        public Transaction build() {
            Transaction transaction = new Transaction();
            JsonObject jsonObject = new JsonObject();
            jsonObject.put("id", id);
            jsonObject.put("updatedDate", updatedDate);
            jsonObject.put("transactionDate", (transactionDate == null)? "" : transactionDate.toString());
            jsonObject.put("state", state);
            jsonObject.put("cardScheme", cardScheme);
            jsonObject.put("merchantId", merchantId);
            jsonObject.put("bin", bin);
            jsonObject.put("provider", provider);
            jsonObject.put("locationId", locationId);
            jsonObject.put("lastFour", lastFour);
            jsonObject.put("merchantName", merchantName);
            jsonObject.put("source", source);
            jsonObject.put("amount", amount);
            jsonObject.put("matchedWith",  matchedWith);
            jsonObject.put("createdDate", (createdDate == null)? "" : createdDate.toString());
            jsonObject.put("updatedDate", (updatedDate == null)? "" : updatedDate.toString());
            transaction.jsonObject = jsonObject;
            return transaction;
        }
    }

    public JsonObject toJson() {
        return this.jsonObject;
    }
}
