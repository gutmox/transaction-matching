package com.gutmox.tx.matching.transactions.api;

import com.gutmox.tx.matching.transactions.domain.MatchTransactions;
import com.gutmox.tx.matching.transactions.domain.Transaction;
import com.gutmox.tx.matching.transactions.domain.TransactionStats;
import io.reactivex.Completable;
import io.reactivex.Single;

import java.util.List;

public interface TransactionsApi {

    Single<List<Transaction>> getAll(String state);

    Single<Transaction> getOne(String transactionIdentifier);

    Single<TransactionStats> getStats();

    Completable match(MatchTransactions fromJson);

    Completable unmatch(MatchTransactions fromJson);
}
