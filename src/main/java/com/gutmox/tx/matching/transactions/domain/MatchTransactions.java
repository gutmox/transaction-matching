package com.gutmox.tx.matching.transactions.domain;

import io.vertx.core.json.JsonObject;

public class MatchTransactions {

    private JsonObject jsonObject;

    private MatchTransactions() {
        super();
    }

    private MatchTransactions(JsonObject jsonObject) {
        this.jsonObject = jsonObject;
    }

    public static MatchTransactions fromJson(JsonObject jsonObject) {
        return new MatchTransactions(jsonObject);
    }

    public String getTransactionOne() {
        return jsonObject.getString("transaction_one");
    }

    public String getTransactionTwo() {
        return jsonObject.getString("transaction_two");
    }
}
