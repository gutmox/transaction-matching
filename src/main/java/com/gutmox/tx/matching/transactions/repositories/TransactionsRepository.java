package com.gutmox.tx.matching.transactions.repositories;

import com.google.inject.Inject;
import com.gutmox.tx.matching.pgclient.PostgresClient;
import com.gutmox.tx.matching.transactions.domain.Transaction;
import com.gutmox.tx.matching.transactions.domain.TransactionState;
import com.gutmox.tx.matching.transactions.domain.TransactionStats;
import io.reactivex.Completable;
import io.reactivex.Single;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class TransactionsRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(TransactionsRepository.class);

    private PostgresClient postgresClient;

    private TransactionDaoMapper transactionDaoMapper;

    private String  selectAll = "SELECT * FROM transactions";
    private String  selectOne = "SELECT * FROM transactions WHERE id='";
    private String  selectByState = "SELECT * FROM transactions WHERE state='";
    private String  stats = "SELECT state, COUNT(state) FROM transactions GROUP BY state";
    private String matched = "UPDATE transactions SET state='MATCHED', matched_with='$1' where id='$2'";
    private String unmatched = "UPDATE transactions SET state='UNMATCHED', matched_with=null where id='$1'";

    @Inject
    public TransactionsRepository(PostgresClient postgresClient, TransactionDaoMapper transactionDaoMapper) {
        this.postgresClient = postgresClient;
        this.transactionDaoMapper = transactionDaoMapper;
    }

    public Single<List<Transaction>> getAll() {

        LOGGER.info("getAll ");

        return postgresClient.createFlowable(selectAll).map(row -> transactionDaoMapper.map(row)).toList();
    }

    public Single<Transaction> getOne(String transactionIdentifier) {

        LOGGER.info("getOne " + transactionIdentifier);

        return postgresClient.createFlowable(selectOne + transactionIdentifier + "'").map(row -> transactionDaoMapper.map(row)).firstElement().toSingle();
    }

    public Single<List<Transaction>> getByState(TransactionState state) {

        LOGGER.info("getByState " + state);

        return postgresClient.createFlowable(selectByState + state + "'").map(row -> transactionDaoMapper.map(row)).toList();
    }

    public Single<TransactionStats> getStats() {

        LOGGER.info("getStats ");
        AtomicInteger matched = new AtomicInteger(0);
        AtomicInteger unmatched = new AtomicInteger(0);

        return postgresClient.createFlowable(stats).map(row -> {
            transactionDaoMapper.stats(matched, unmatched, row);
            return TransactionStats.builder()
                    .withMatched(matched.get())
                    .withUnmatched(unmatched.get())
                    .build();
        }).lastOrError();


    }

    public Completable match(String transactionOne, String transactionTwo) {

        return postgresClient.transaction(matched.replace("$1", transactionOne).replace("$2", transactionTwo),
                matched.replace("$1", transactionTwo).replace("$2", transactionOne));
    }

    public Completable unmatch(String transactionOne, String transactionTwo) {

        return postgresClient.transaction(unmatched.replace("$1", transactionOne),
                unmatched.replace("$1", transactionTwo));
    }
}
