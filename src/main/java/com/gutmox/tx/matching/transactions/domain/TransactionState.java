package com.gutmox.tx.matching.transactions.domain;

public enum TransactionState {
    UNMATCHED,
    MATCHED
}
