package com.gutmox.tx.matching.transactions.domain;

import io.vertx.core.json.JsonObject;

public class Matched {

    private JsonObject jsonObject;

    private Matched() {
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private boolean matched;

        private Builder() {
        }


        public Builder withMatched(boolean matched) {
            this.matched = matched;
            return this;
        }

        public Matched build() {
            Matched matched = new Matched();
            JsonObject jsonObject = new JsonObject();
            jsonObject.put("matched", this.matched);
            matched.jsonObject = jsonObject;
            return matched;
        }
    }

    public JsonObject toJson() {
        return this.jsonObject;
    }
}
