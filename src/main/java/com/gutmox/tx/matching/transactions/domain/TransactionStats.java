package com.gutmox.tx.matching.transactions.domain;

import io.vertx.core.json.JsonObject;

public class TransactionStats {

    private JsonObject jsonObject;

    private TransactionStats() {
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {

        private Integer matched;

        private Integer unmatched;

        private Builder() {
        }

        public Builder withMatched(Integer matched) {
            this.matched = matched;
            return this;
        }

        public Builder withUnmatched(Integer unmatched) {
            this.unmatched = unmatched;
            return this;
        }

        public TransactionStats build() {
            TransactionStats transactionStats = new TransactionStats();
            JsonObject jsonObject = new JsonObject();
            jsonObject.put("matched", matched);
            jsonObject.put("unmatched", unmatched);
            transactionStats.jsonObject = jsonObject;
            return transactionStats;
        }
    }

    public JsonObject toJson() {
        return this.jsonObject;
    }
}
