package com.gutmox.tx.matching.pgclient;

import io.reactiverse.reactivex.pgclient.Row;

public interface DaoMapper<T> {

    T map(Row row);
}
