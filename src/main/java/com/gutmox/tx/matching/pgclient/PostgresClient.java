package com.gutmox.tx.matching.pgclient;

import io.reactiverse.pgclient.PgPoolOptions;
import io.reactiverse.reactivex.pgclient.*;
import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.reactivex.core.Vertx;

public class PostgresClient {

    private static final Logger LOGGER = LoggerFactory.getLogger(PostgresClient.class);

    private static PgPool client;

    public static synchronized void initialise(Vertx vertx) {
        PgPoolOptions options = new PgPoolOptions()
                .setPort(5432)
                .setHost("localhost")
                .setDatabase("postgres")
                .setUser("sandbox")
                .setPassword("sandbox")
                .setMaxSize(5);

        // Create the pooled client
        client = PgClient.pool(vertx, options);

    }

    public Flowable<Row> createFlowable(String sql) {
        return client.rxBegin()
                .flatMapPublisher(tx -> tx.rxPrepare(sql)
                        .flatMapPublisher(preparedQuery -> {
                            // Fetch 50 rows at a time
                            PgStream<io.reactiverse.reactivex.pgclient.Row> stream = preparedQuery.createStream(50, Tuple.tuple());
                            return stream.toFlowable();
                        })
                        .doAfterTerminate(tx::commit));
    }

    public Completable transaction(String... queries) {

        return client.rxBegin()
                .flatMapCompletable(tx -> tx
                        .rxQuery(queries[0])
                        .flatMap(result -> tx.rxQuery(queries[1]))
                        .flatMapCompletable(result -> tx.rxCommit()));
    }
}
