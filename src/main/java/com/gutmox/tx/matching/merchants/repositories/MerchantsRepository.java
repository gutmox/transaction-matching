package com.gutmox.tx.matching.merchants.repositories;

import com.google.inject.Inject;
import com.gutmox.tx.matching.merchants.domain.Merchant;
import com.gutmox.tx.matching.pgclient.PostgresClient;
import io.reactivex.Single;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

import java.util.List;

public class MerchantsRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(MerchantsRepository.class);

    private PostgresClient postgresClient;

    private MerchantDaoMapper merchantDaoMapper;

    private String  selectAll = "SELECT * FROM merchants";
    private String  selectOne = "SELECT * FROM merchants WHERE id='";
    private String  selectByName = "SELECT * FROM merchants WHERE name='";

    @Inject
    public MerchantsRepository(PostgresClient postgresClient, MerchantDaoMapper merchantDaoMapper) {
        this.postgresClient = postgresClient;
        this.merchantDaoMapper = merchantDaoMapper;
    }

    public Single<List<Merchant>> getAll() {

        LOGGER.info("getAll ");

        return postgresClient.createFlowable(selectAll).map(row -> merchantDaoMapper.map(row)).toList();
    }

    public Single<Merchant> getOne(String merchantIdentifier) {

        LOGGER.info("getOne " + merchantIdentifier);

        return postgresClient.createFlowable(selectOne + merchantIdentifier + "'").map(row -> merchantDaoMapper.map(row)).firstElement().toSingle();
    }

    public Single<List<Merchant>> getByName(String name) {

        LOGGER.info("getByName " + name);

        return postgresClient.createFlowable(selectByName + name + "'").map(row -> merchantDaoMapper.map(row)).toList();
    }
}
