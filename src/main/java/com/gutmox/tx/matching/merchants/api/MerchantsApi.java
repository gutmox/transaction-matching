package com.gutmox.tx.matching.merchants.api;


import com.gutmox.tx.matching.merchants.domain.Merchant;
import io.reactivex.Single;

import java.util.List;

public interface MerchantsApi {

    Single<List<Merchant>> getAll(String name);

    Single<Merchant> getOne(String merchantIdentifier);
}
