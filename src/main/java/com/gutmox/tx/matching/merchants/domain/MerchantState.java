package com.gutmox.tx.matching.merchants.domain;

public enum MerchantState {
    ACTIVE,
    INACTIVE
}
