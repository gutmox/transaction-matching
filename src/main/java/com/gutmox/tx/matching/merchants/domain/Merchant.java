package com.gutmox.tx.matching.merchants.domain;

import io.vertx.core.json.JsonObject;

import java.time.LocalDateTime;

public class Merchant {

    private JsonObject jsonObject;

    private Merchant() {
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private String id;
        private String name;
        private String logoUrl;
        private MerchantState state;
        private LocalDateTime createdDate;
        private LocalDateTime updatedDate;

        private Builder() {
        }

        public Builder withId(String id) {
            this.id = id;
            return this;
        }

        public Builder withName(String name) {
            this.name = name;
            return this;
        }

        public Builder withLogoUrl(String logoUrl) {
            this.logoUrl = logoUrl;
            return this;
        }

        public Builder withState(MerchantState state) {
            this.state = state;
            return this;
        }

        public Builder withCreatedDate(LocalDateTime createdDate) {
            this.createdDate = createdDate;
            return this;
        }

        public Builder withUpdatedDate(LocalDateTime updatedDate) {
            this.updatedDate = updatedDate;
            return this;
        }

        public Merchant build() {
            Merchant merchant = new Merchant();
            JsonObject jsonObject = new JsonObject();
            jsonObject.put("id", id);
            jsonObject.put("name", name);
            jsonObject.put("logoUrl", logoUrl);
            jsonObject.put("state", state);
            jsonObject.put("createdDate", (createdDate == null)? "" : createdDate.toString());
            jsonObject.put("updatedDate", (updatedDate == null)? "" : updatedDate.toString());

            merchant.jsonObject = jsonObject;
            return merchant;
        }
    }

    public JsonObject toJson() {
        return this.jsonObject;
    }
}
