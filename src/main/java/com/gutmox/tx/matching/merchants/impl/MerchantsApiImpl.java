package com.gutmox.tx.matching.merchants.impl;

import com.google.inject.Inject;
import com.gutmox.tx.matching.merchants.api.MerchantsApi;
import com.gutmox.tx.matching.merchants.repositories.MerchantsRepository;
import com.gutmox.tx.matching.merchants.domain.Merchant;
import io.reactivex.Single;

import java.util.List;
import java.util.Optional;

public class MerchantsApiImpl implements MerchantsApi {

    private MerchantsRepository merchantsRepository;

    @Inject
    public MerchantsApiImpl(MerchantsRepository merchantRepository) {
        this.merchantsRepository = merchantRepository;
    }

    @Override
    public Single<List<Merchant>> getAll(String name) {
        if (Optional.ofNullable(name).isPresent()){
            return merchantsRepository.getByName(name);
        }else {
            return merchantsRepository.getAll();
        }
    }

    @Override
    public Single<Merchant> getOne(String merchantIdentifier) {
        return merchantsRepository.getOne(merchantIdentifier);
    }
}
