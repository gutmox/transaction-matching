package com.gutmox.tx.matching.merchants.repositories;

import com.gutmox.tx.matching.merchants.domain.Merchant;
import com.gutmox.tx.matching.merchants.domain.MerchantState;
import com.gutmox.tx.matching.pgclient.DaoMapper;
import io.reactiverse.reactivex.pgclient.Row;

public class MerchantDaoMapper implements DaoMapper<Merchant> {

    @Override
    public Merchant map(Row row){
        return Merchant.builder()
                .withId(row.getUUID("id").toString())
                .withName(row.getString("name"))
                .withLogoUrl(row.getString("logo_url"))
                .withState(MerchantState.valueOf(row.getString("state")))
                .withCreatedDate(row.getLocalDateTime("created_date"))
                .withUpdatedDate(row.getLocalDateTime("updated_date"))
                .build();
    }
}
