package com.gutmox.tx.matching.routing;

import com.google.inject.Inject;
import com.gutmox.tx.matching.auth.handlers.AuthHandler;
import com.gutmox.tx.matching.handlers.MerchantsHandler;
import com.gutmox.tx.matching.handlers.StatusHandler;
import com.gutmox.tx.matching.handlers.TransactionsHandler;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.reactivex.ext.web.Router;
import io.vertx.reactivex.ext.web.handler.BodyHandler;
import io.vertx.reactivex.ext.web.handler.ResponseTimeHandler;

public class Routing {

    private static final Logger LOGGER = LoggerFactory.getLogger(Routing.class);

    private final StatusHandler statusHandler;

    private final MerchantsHandler merchantsHandler;

    private final TransactionsHandler transactionsHandler;

    private final AuthHandler authHandler;

    private Router router;

    private static String loginURI  = "/auth/login";
    private static String logoutURI = "/auth/logout";
    private static String signUpURI = "/auth/signup";

    private static final String MERCHANT_URL = "/merchants";
    private static final String TRANSACTIONS_URL = "/transactions";
    private static final String TRANSACTIONS_STATS_URL = "/transactions/stats";
    private static final String TRANSACTIONS_MATCH_URL = "/transactions/match";
    private static final String TRANSACTIONS_UNMATCH_URL = "/transactions/unmatch";

    @Inject
    public Routing(StatusHandler statusHandler,
                   MerchantsHandler merchantsHandler,
                   TransactionsHandler transactionsHandler,
                   AuthHandler authHandler) {
        this.statusHandler = statusHandler;
        this.merchantsHandler = merchantsHandler;
        this.transactionsHandler = transactionsHandler;
        this.authHandler = authHandler;
    }

    public Router getRouter() {

        router.route().handler(ResponseTimeHandler.create());

        router.get("/status").handler(statusHandler);

        routeMerchantEndpoints();
        routeTransactionsEndpoints();
        authRouting();

        LOGGER.debug("Routing Done");

        return router;
    }

    private void routeTransactionsEndpoints() {
        router.route(TRANSACTIONS_URL + "/*").handler(authHandler::authenticate);
        router.get(TRANSACTIONS_URL).handler(transactionsHandler::getAll);
        router.get(TRANSACTIONS_STATS_URL).handler(transactionsHandler::getStats);
        router.get(TRANSACTIONS_URL + "/:transactionIdentifier").handler(transactionsHandler::getOne);
        long bodyLimit = 1024;
        router.post(TRANSACTIONS_MATCH_URL).handler(BodyHandler.create().setBodyLimit(bodyLimit * bodyLimit));
        router.post(TRANSACTIONS_MATCH_URL).handler(transactionsHandler::match);
        router.post(TRANSACTIONS_UNMATCH_URL).handler(BodyHandler.create().setBodyLimit(bodyLimit * bodyLimit));
        router.post(TRANSACTIONS_UNMATCH_URL).handler(transactionsHandler::unmatch);
    }

    private void routeMerchantEndpoints() {
        router.route(MERCHANT_URL + "/*").handler(authHandler::authenticate);
        router.get(MERCHANT_URL).handler(merchantsHandler::getAll);
        router.get(MERCHANT_URL + "/:merchantIdentifier").handler(merchantsHandler::getOne);
    }

    private void authRouting() {
        long bodyLimit = 1024;
        router.post(loginURI).handler(BodyHandler.create().setBodyLimit(bodyLimit * bodyLimit));
        router.post(loginURI).handler(authHandler::login);
        router.get(logoutURI).handler(authHandler::logout);
        router.post(signUpURI).handler(BodyHandler.create().setBodyLimit(bodyLimit * bodyLimit));
        router.post(signUpURI).handler(authHandler::signUp);
    }

    public void setRouter(Router router) {
        this.router = router;
    }
}
