package com.gutmox.tx.matching.auth.repositories;

import com.gutmox.tx.matching.config.VertxConfiguration;
import io.reactivex.Single;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.reactivex.ext.mongo.MongoClient;

import javax.inject.Inject;

public class AuthRepositoryMongoImpl implements AuthRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthRepositoryMongoImpl.class);

    private MongoClient mongoClient;

    private String collection = "auth_users";

    private String userName = "userName";

    private String mongoIdentifier = "_id";

    @Inject
    public AuthRepositoryMongoImpl() {

        this(VertxConfiguration.authMongoClient());
    }

    public AuthRepositoryMongoImpl(MongoClient mongoClient) {
        this.mongoClient = mongoClient;
    }

    @Override
    public Single<String> save(JsonObject userAuth) {

        LOGGER.info("user to save  : " + userAuth);

        userAuth.put(mongoIdentifier, userAuth.getString(userName));

        return mongoClient.rxSave(collection, userAuth);
    }

    @Override
    public Single<JsonObject> find(String userName) {

        LOGGER.info(this.userName  + " :  " + userName);

        return mongoClient.rxFindOne(collection, new JsonObject().put(mongoIdentifier, userName), null);
    }
}
