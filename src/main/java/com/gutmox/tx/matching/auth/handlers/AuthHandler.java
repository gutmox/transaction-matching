package com.gutmox.tx.matching.auth.handlers;

import com.google.inject.Inject;
import com.gutmox.tx.matching.auth.api.AuthenticationApi;
import com.gutmox.tx.matching.auth.domain.JWT;
import com.gutmox.tx.matching.auth.domain.User;
import com.gutmox.tx.matching.auth.impl.AuthenticationApiImpl;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.reactivex.ext.web.RoutingContext;

import java.util.Optional;

public class AuthHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthHandler.class);

    private AuthenticationApi authenticationApi;

    @Inject
    public AuthHandler(AuthenticationApiImpl authenticationApi) {
        this.authenticationApi = authenticationApi;
    }

    public void authenticate(RoutingContext event) {

        Optional<String> authorisationToken = Optional.ofNullable(event.request().headers().get(HttpHeaders.AUTHORIZATION));
        if (! authorisationToken.isPresent()){
            errorPayload(event, "authorization token not indicated");
        }

        authenticationApi.authenticate(JWT.builder().withJwtToken(authorisationToken.get()).build()).doOnError(err ->{
            errorPayload(event, "authorization token not valid");
        }).subscribe( res -> event.next());
    }

    private void errorPayload(RoutingContext event, String errorMessage) {
        event.response().putHeader("content-type", "application/json")
                .end(new JsonObject().put("error", errorMessage).encode());
    }

    public void login(RoutingContext event) {

        LOGGER.info("Body : " + event.getBodyAsString());

        authenticationApi.login(User.fromJson(event.getBodyAsJson())).doOnError(err ->{
            event.response().putHeader("content-type", "application/json")
                    .end(new JsonObject().put("error", err.getMessage()).encode());
        }).subscribe(jwt ->{

            LOGGER.info("JWT : " + jwt.toJson());

            event.response().putHeader("content-type", "application/json")
                    .end(jwt.toJson().encode());
        });
    }

    public void logout(RoutingContext event) {

        String authorization = event.request().headers().get(HttpHeaders.AUTHORIZATION);

        authenticationApi.logout(JWT.builder().withJwtToken(authorization).build())
                .doOnError(err ->{
                    event.response().putHeader("content-type", "application/json")
                            .end(new JsonObject().put("error", err.getMessage()).encode());
                })
                .subscribe(res ->{
                    event.response().putHeader("content-type", "application/json").end();
                });
    }

    public void signUp(RoutingContext event) {

        LOGGER.info("Body : " + event.getBodyAsString());

        authenticationApi.signUp(User.fromJson(event.getBodyAsJson()))
                .doOnError(err -> {
                    event.response().putHeader("content-type", "application/json")
                            .end(new JsonObject().put("error", err.getMessage()).encode());
                }).subscribe(jwt -> {

            LOGGER.info("JWT : " + jwt.toJson());

            event.response().putHeader("content-type", "application/json")
                    .end(jwt.toJson().encode());
        });

    }
}
