package com.gutmox.tx.matching.auth.api;


import com.gutmox.tx.matching.auth.domain.JWT;
import com.gutmox.tx.matching.auth.domain.User;
import io.reactivex.Single;
import io.vertx.core.json.JsonObject;

public interface AuthenticationApi {

    Single<JWT> login(User user);

    Single<JWT> signUp(User fromJson);

    Single<Void> logout(JWT jwt);

    Single<JsonObject> authenticate(JWT jwt);
}