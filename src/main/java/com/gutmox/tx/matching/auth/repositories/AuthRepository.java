package com.gutmox.tx.matching.auth.repositories;

import io.reactivex.Single;
import io.vertx.core.json.JsonObject;

public interface AuthRepository {

    Single<String> save(JsonObject userAuth);

    Single<JsonObject> find(String userName);

}
