package com.gutmox.tx.matching.auth.impl;

import com.google.inject.Inject;
import com.gutmox.tx.matching.auth.api.AuthenticationApi;
import com.gutmox.tx.matching.auth.domain.JWT;
import com.gutmox.tx.matching.auth.domain.User;
import com.gutmox.tx.matching.auth.domain.UserAuth;
import com.gutmox.tx.matching.auth.jwt.JwtManager;
import com.gutmox.tx.matching.auth.repositories.AuthRepository;
import com.gutmox.tx.matching.auth.repositories.AuthRepositoryMongoImpl;
import com.gutmox.tx.matching.config.PropertiesManager;
import io.reactivex.Single;
import io.vertx.core.json.JsonObject;
import org.mindrot.jbcrypt.BCrypt;

public class AuthenticationApiImpl implements AuthenticationApi {

    private AuthRepository authRepository;

    private JwtManager jwtManager;

    @Inject
    public AuthenticationApiImpl(AuthRepositoryMongoImpl authRepository, JwtManager jwtManager) {
        this.authRepository = authRepository;
        this.jwtManager = jwtManager;
    }

    @Override
    public Single<JWT> login(User user) {
        return authRepository.find(user.getUserName()).map(res -> {
            UserAuth userAuth = UserAuth.fromJson(res);

            if (res == null ||
                    !BCrypt.checkpw(user.getPassword(), userAuth.getHashedPassword())) {

                throw new RuntimeException("Invalid Credentials");
            }

            return JWT.builder().withJwtToken(jwtManager.generateToken(userAuth.getUserName())).build();
        });
    }

    @Override
    public Single<JWT> signUp(User user) {
        return authRepository.save(UserAuth.builder()
                .withUserName(user.getUserName())
                .withHashedPassword(BCrypt.hashpw(user.getPassword(), BCrypt.gensalt())).build().toJson())
                .flatMap(saved -> Single.just(JWT.builder().withJwtToken(jwtManager.generateToken(user.getUserName())).build()));

    }

    @Override
    public Single<Void> logout(JWT jwt) {
        jwtManager.authenticate(jwt.toJson()).subscribe(user ->
                jwtManager.blacklistToken(jwt.getJwtToken(), PropertiesManager.getInstance().getIntValue("jwt.expire")));
        return Single.just(null);
    }

    @Override
    public Single<JsonObject> authenticate(JWT jwt) {
        return jwtManager.authenticate(jwt.toJson()).map(authenticated -> authenticated.principal());
    }
}
