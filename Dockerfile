###
#
# To build:
#
#  docker build -t transaction-matching .
#
# To run:
#
#  docker run -t -i -p 8080:8080 transaction-matching
#
###

FROM java:8-jre

ENV DIST_FILE build/distributions/transaction-matching-1.0-SNAPSHOT.tar

# Set the location of the verticles
ENV VERTICLE_HOME /opt/verticles

EXPOSE 8080

COPY $DIST_FILE $VERTICLE_HOME/

WORKDIR $VERTICLE_HOME

RUN tar -xvf transaction-matching-1.0-SNAPSHOT.tar

ENTRYPOINT ["sh", "-c"]
CMD ["./transaction-matching-1.0-SNAPSHOT/bin/transaction-matching run MainVerticle"]
