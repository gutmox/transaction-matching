# Transaction matching

[![pipeline status](https://gitlab.com/gutmox/transaction-matching/badges/master/pipeline.svg)](https://gitlab.com/gutmox/transaction-matching/commits/master)

Simple Transaction matching REST API.

## Built With

* [Java 8](http://www.oracle.com/technetwork/java/javase/10-relnote-issues-4108729.html) - Code language 
* [Vert.x](https://vertx.io) - REST library used
* [RX Java 2](https://github.com/ReactiveX/RxJava) - Reactive Extensions for the JVM library
* [Reactive-pg-client](https://github.com/reactiverse/reactive-pg-client) - Reactive Postgres high performance client
* [Gradle](https://gradle.org) - Build tool
* [Guice](https://github.com/google/guice) - Dependency injection


## Setup

Just run `docker-compose up -d` to start the  Postgres and MongoDb databases.

I added MongoDb to store users to be able to sign in, login and validate a JWT token.

Run the sql files under the /db directory, against Postgres to populate it with Test data.

## JWT Authentication

Follow next steps :

* Create an user through the signup endpoint. As this is an internal tool, this endpoint should be protected by IP / only accesible by admins
* Login with the previous obtained user, getting a JWT token.
* Using the returned token as Authorization Header, the merchants and transactions API is accessible.

#### User Sign up

```
curl -X POST \
  http://localhost:8080/auth/signup \
  -H 'Content-Type: application/json' \
  -H 'cache-control: no-cache' \
  -d '{
  "userName": "userName",
  "password": "password"
}'
``` 

#### User login to get a JWT Token

```
curl -X POST \
  http://localhost:8080/auth/login \
  -H 'Content-Type: application/json' \
  -H 'cache-control: no-cache' \
  -d '{
  "userName": "userName",
  "password": "password"
}'
```

## Merchants and Transaction endpoints

* `GET /merchants?name={name}` - list merchant, not including locations (lightweight, top level)
```
Examples:

curl -X GET \
  http://localhost:8080/merchants \
  -H 'Authorization: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyTmFtZSI6InVzZXJOYW1lIiwiaWF0IjoxNTUzMDM0Nzk4LCJleHAiOjE1NTMyMDc1OTgsImF1ZCI6IlVzZXIiLCJpc3MiOiJIZXkgQ29ycC4ifQ.bT6aeXqspKUUkjWYn3OX2I58ZHqsbwt7atZY3wc99Qo' \
  -H 'cache-control: no-cache'
  
   
curl -X GET \
  'http://localhost:8080/merchants?name=EAT.' \
  -H 'Authorization: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyTmFtZSI6InVzZXJOYW1lIiwiaWF0IjoxNTUzMDM0Nzk4LCJleHAiOjE1NTMyMDc1OTgsImF1ZCI6IlVzZXIiLCJpc3MiOiJIZXkgQ29ycC4ifQ.bT6aeXqspKUUkjWYn3OX2I58ZHqsbwt7atZY3wc99Qo' \
  -H 'cache-control: no-cache'
```

* `GET /merchants/{id}` - grab a merchant by id, including locations
```
Example:

curl -X GET \
  http://localhost:8080/merchants/92e50938-1aec-4ca7-995d-dc79ae0428fb \
  -H 'Authorization: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyTmFtZSI6InVzZXJOYW1lIiwiaWF0IjoxNTUzMDM0Nzk4LCJleHAiOjE1NTMyMDc1OTgsImF1ZCI6IlVzZXIiLCJpc3MiOiJIZXkgQ29ycC4ifQ.bT6aeXqspKUUkjWYn3OX2I58ZHqsbwt7atZY3wc99Qo' \
  -H 'cache-control: no-cache' 
```

* `GET /transactions/stats` - get a map of state: count of transactions in said state i.e. `{ "MATCHED": 1, "UNMATCHED": 2 }`
```
Example:

curl -X GET \
  http://localhost:8080/transactions/stats \
  -H 'Authorization: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyTmFtZSI6InVzZXJOYW1lIiwiaWF0IjoxNTUzMDM0Nzk4LCJleHAiOjE1NTMyMDc1OTgsImF1ZCI6IlVzZXIiLCJpc3MiOiJIZXkgQ29ycC4ifQ.bT6aeXqspKUUkjWYn3OX2I58ZHqsbwt7atZY3wc99Qo' \
  -H 'cache-control: no-cache' 
```

* `GET /transactions?state={state}` - get a list of transactions in a specific state (i.e. MATCHED/UNMATCHED)
```
Examples:

curl -X GET \
  http://localhost:8080/transactions \
  -H 'Authorization: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyTmFtZSI6InVzZXJOYW1lIiwiaWF0IjoxNTUzMDM0Nzk4LCJleHAiOjE1NTMyMDc1OTgsImF1ZCI6IlVzZXIiLCJpc3MiOiJIZXkgQ29ycC4ifQ.bT6aeXqspKUUkjWYn3OX2I58ZHqsbwt7atZY3wc99Qo' \
  -H 'cache-control: no-cache'
  
   
curl -X GET \
  'http://localhost:8080/transactions?state=UNMATCHED' \
  -H 'Authorization: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyTmFtZSI6InVzZXJOYW1lIiwiaWF0IjoxNTUzMDM0Nzk4LCJleHAiOjE1NTMyMDc1OTgsImF1ZCI6IlVzZXIiLCJpc3MiOiJIZXkgQ29ycC4ifQ.bT6aeXqspKUUkjWYn3OX2I58ZHqsbwt7atZY3wc99Qo' \
  -H 'cache-control: no-cache'
   
```

* `GET /transactions/{id}` - get a transaction, state and candidates
```
Example:

curl -X GET \
  http://localhost:8080/transactions/6939646b-c894-4406-bed9-7de87221e79e \
  -H 'Authorization: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyTmFtZSI6InVzZXJOYW1lIiwiaWF0IjoxNTUzMDM0Nzk4LCJleHAiOjE1NTMyMDc1OTgsImF1ZCI6IlVzZXIiLCJpc3MiOiJIZXkgQ29ycC4ifQ.bT6aeXqspKUUkjWYn3OX2I58ZHqsbwt7atZY3wc99Qo' \
  -H 'cache-control: no-cache'
   
```

* `POST /transactions/match` - match two transactions together
```
Example: 

curl -X POST \
  http://localhost:8080/transactions/match \
  -H 'Authorization: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyTmFtZSI6InVzZXJOYW1lIiwiaWF0IjoxNTUzMDM0Nzk4LCJleHAiOjE1NTMyMDc1OTgsImF1ZCI6IlVzZXIiLCJpc3MiOiJIZXkgQ29ycC4ifQ.bT6aeXqspKUUkjWYn3OX2I58ZHqsbwt7atZY3wc99Qo' \
  -H 'Content-Type: application/json' \
  -d '{
    "transaction_one": "6939646B-C894-4406-BED9-7DE87221E79E",
    "transaction_two": "4D74E4BE-95FD-41B1-B8EF-FD06A7F086B0"
}'
```

* `POST /transactions/unmatch` - unmatch two transactions

```
curl -X POST \
  http://localhost:8080/transactions/unmatch \
  -H 'Authorization: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyTmFtZSI6InVzZXJOYW1lIiwiaWF0IjoxNTUzMDM0Nzk4LCJleHAiOjE1NTMyMDc1OTgsImF1ZCI6IlVzZXIiLCJpc3MiOiJIZXkgQ29ycC4ifQ.bT6aeXqspKUUkjWYn3OX2I58ZHqsbwt7atZY3wc99Qo' \
  -H 'Content-Type: application/json' \
  -d '{
    "transaction_one": "6939646B-C894-4406-BED9-7DE87221E79E",
    "transaction_two": "4D74E4BE-95FD-41B1-B8EF-FD06A7F086B0"
}'

```

To match two transactions - the state column should be updated to 'MATCHED' and the matched_with column should be populated with the opposite matching transaction id.

## Get merchants Flow sequence

This is the how the sequence is done for a GET /merchants flow. It's similar for other cases, 
and the purpose of the diagram is to illustrate how the software layers are strustured. 
I followed an 'usual' Vertx structure.
   

```mermaid
sequenceDiagram
    Client->> Routing: GET /transactions
    Routing->> MerchantsHandler: getAll
    MerchantsHandler ->> MerchantsApiImpl: getAll
    MerchantsApiImpl ->> MerchantsRepository: getAll()
    MerchantsRepository ->> PostgresClient: createFlowable(String sql)
```

## Package organisation

|package   	    | description|
|---	        |---	            |
|com.gutmox.tx.matching    | Main Vertx Verticle|
|com.gutmox.tx.matching.guice    | Glueing Guice with Vertx|
|com.gutmox.tx.matching.handlers    | Vertx Handlers|
|com.gutmox.tx.matching.merchants.api    | Merchants Java API|
|com.gutmox.tx.matching.merchants.domain    | Domain entities for Merchants Java API|
|com.gutmox.tx.matching.merchants.impl    | Merchants Java API Implementation|
|com.gutmox.tx.matching.merchants.repositories    | Merchants repository|
|com.gutmox.tx.matching.pgclient   | reactive-pg-client wrapper|
|com.gutmox.tx.matching.routing   | Vert.x routing where endpoints are defined and linked to handlers|
|com.gutmox.tx.matching.transactions.api    | Transactions Java API|
|com.gutmox.tx.matching.transactions.domain    | Domain entities for Transactions Java API|
|com.gutmox.tx.matching.transactions.impl    | Transactions Java API Implementation|
|com.gutmox.tx.matching.transactions.repositories    | Transactions repository|


## Running It

## From command line

```
./gradlew clean run
```

## From your IDE

Configure Launcher this way:

![IntelliJ Config](https://gitlab.com/gutmox/transaction-matching/raw/master/src/main/resources/intellij-run.png)


## Building
```
./gradlew clean build
```

## Packaging

```
./gradlew clean assemble
```


## Docker

### build

```
docker build -t transaction-matching .
 ```
### run

```
docker run -t -i -p 8080:8080 transaction-matching
```